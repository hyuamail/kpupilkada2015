package id.sch.smktelkom_mlg.visionet.kpupilkada2015.model;

/**
 * Created by hyuam on 16/11/2016.
 */

public class ResponseCandidacy
{
    public String comm;
    public String wilayah;
    public Candidacy data;
    public String cmd;

    @Override
    public String toString()
    {
        return "ResponseCandidacy{" +
                "comm='" + comm + '\'' +
                ", wilayah='" + wilayah + '\'' +
                ", data=" + data +
                ", cmd='" + cmd + '\'' +
                '}';
    }
}
