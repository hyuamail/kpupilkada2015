package id.sch.smktelkom_mlg.visionet.kpupilkada2015;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.util.List;

import id.sch.smktelkom_mlg.visionet.kpupilkada2015.model.CandidacyList;
import id.sch.smktelkom_mlg.visionet.kpupilkada2015.model.ResponseCandidacy;
import id.sch.smktelkom_mlg.visionet.kpupilkada2015.service.IKPUDataAPI;
import id.sch.smktelkom_mlg.visionet.kpupilkada2015.service.KPUDataAPI;
import id.sch.smktelkom_mlg.visionet.kpupilkada2015.util.AssetsReader;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
{

    public static final String LOGTAG = "FLOW";
    private IKPUDataAPI mService;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        AssetsReader assetsReader = new AssetsReader(this);
        assetsReader.insertCandidacyList("candidacy.csv");

        List<CandidacyList> list = CandidacyList.listAll(CandidacyList.class);
        Log.d(LOGTAG, "onCreate: " + list.toString());

        mService = KPUDataAPI.getServiceAPI();
        for (int i = 0; i < list.size(); i++)
        {
            downloadCandidacy(list.get(i).candidacy_id);
        }
    }

    private void downloadCandidacy(final String candidacy_id)
    {
        Log.d(LOGTAG, "downloadCandidacy: " + candidacy_id + "|");
        try
        {
            KPUDataAPI.callDataCandidacy(mService, candidacy_id, new Callback()
            {
                @Override
                public void onResponse(Call call, Response response)
                {
                    Log.d(LOGTAG, "Response:" + candidacy_id + ":" + call + " :" +
                            response.body().toString());
                    ResponseCandidacy responseCandidacy = (ResponseCandidacy) response.body();
                    responseCandidacy.data.save();
                }

                @Override
                public void onFailure(Call call, Throwable t)
                {
                    Log.e(LOGTAG, "Error:" + candidacy_id + ":" + call.toString(), t);
                }
            });
        } catch (IOException e)
        {
            Log.e(LOGTAG, "Error", e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
