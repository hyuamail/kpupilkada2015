package id.sch.smktelkom_mlg.visionet.kpupilkada2015.service;

import id.sch.smktelkom_mlg.visionet.kpupilkada2015.model.ResponseCandidacy;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by hyuam on 12/11/2016.
 */

public interface IKPUDataAPI
{
    @GET("api.php?cmd=candidacy")
    Call<ResponseCandidacy> getCandidacy(@Query("candidacy_id") String id);
}
