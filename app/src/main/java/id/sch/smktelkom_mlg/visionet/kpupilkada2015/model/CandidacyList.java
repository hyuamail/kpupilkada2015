package id.sch.smktelkom_mlg.visionet.kpupilkada2015.model;

import com.orm.SugarRecord;

/**
 * Created by hyuam on 17/11/2016.
 */

public class CandidacyList extends SugarRecord
{
    public String candidacy_id;
    public String constituency_id;
    public String v_order;

    public CandidacyList()
    {
    }

    public CandidacyList(String csvData)
    {
        String[] data = csvData.split("[,]");
        candidacy_id = data[0];
        constituency_id = data[1];
        v_order = data[2];
    }

    @Override
    public String toString()
    {
        return "CandidacyList{" +
                "candidacy_id='" + candidacy_id + '\'' +
                ", constituency_id='" + constituency_id + '\'' +
                ", v_order='" + v_order + '\'' +
                '}';
    }
}
