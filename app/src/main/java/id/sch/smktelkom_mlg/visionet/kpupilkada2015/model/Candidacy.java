package id.sch.smktelkom_mlg.visionet.kpupilkada2015.model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by hyuam on 16/11/2016.
 */

public class Candidacy extends SugarRecord
{
    public String election_id;
    public String constituency_id;
    public String pro_id;
    public String kab_id;
    public String provinsi;
    public String dapil;
    public String nourut;
    public String calon;
    public String calon_id;
    public String wakil;
    public String wakil_id;
    public String type;
    public List<String> endorsement;

    @Override
    public String toString()
    {
        return "Candidacy{" +
                "election_id='" + election_id + '\'' +
                ", constituency_id='" + constituency_id + '\'' +
                ", pro_id='" + pro_id + '\'' +
                ", kab_id='" + kab_id + '\'' +
                ", provinsi='" + provinsi + '\'' +
                ", dapil='" + dapil + '\'' +
                ", nourut='" + nourut + '\'' +
                ", calon='" + calon + '\'' +
                ", calon_id='" + calon_id + '\'' +
                ", wakil='" + wakil + '\'' +
                ", wakil_id='" + wakil_id + '\'' +
                ", type='" + type + '\'' +
                ", endorsement=" + endorsement +
                '}';
    }
}
