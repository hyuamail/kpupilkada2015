package id.sch.smktelkom_mlg.visionet.kpupilkada2015.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import id.sch.smktelkom_mlg.visionet.kpupilkada2015.model.ResponseCandidacy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hyuam on 12/11/2016.
 */

public class KPUDataAPI
{
    public static IKPUDataAPI getServiceAPI()
    {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://data.kpu.go.id/open/v1/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(IKPUDataAPI.class);
    }

    public static void callDataCandidacy(IKPUDataAPI service, String id, Callback callback)
            throws IOException
    {
        Call<ResponseCandidacy> callCandidacy = service.getCandidacy(id);
        callCandidacy.enqueue(callback);
    }
}
